/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author Branko
 */
public class Sastojak {
    
    public Sastojci sastojci;
    public int kolicina;
    public Mera mera;

    public Sastojak(Sastojci sastojci, int kolicina, Mera mera) {
        this.sastojci = sastojci;
        this.kolicina = kolicina;
        this.mera = mera;
    }

    @Override
    public String toString() {
        return "Sastojak{" + "sastojci=" + sastojci + ", kolicina=" + kolicina + ", mera=" + mera + '}';
    }

    public Sastojci getSastojci() {
        return sastojci;
    }

    public void setSastojci(Sastojci sastojci) {
        this.sastojci = sastojci;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public Mera getMera() {
        return mera;
    }

    public void setMera(Mera mera) {
        this.mera = mera;
    }
    
}
