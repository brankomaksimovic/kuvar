/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;

/**
 *
 * @author Branko
 */
public class Recepti {

    public String naziv;
    ArrayList<Sastojak> sastojak;
    
    public String getNaziv() {
        return naziv;
    }

    public Recepti() {
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Sastojak> getSastojak() {
        return sastojak;
    }

    public void setSastojak(ArrayList<Sastojak> sastojak) {
        this.sastojak = sastojak;
    }

    public Recepti(String naziv, ArrayList<Sastojak> sastojak) {
        this.naziv = naziv;
        this.sastojak = sastojak;
    }

    /*@Override
    public String toString() {
        return naziv;
    }*/

    @Override
    public String toString() {
        return naziv;
    }
    
    

    public Recepti(String naziv) {
        this.naziv = naziv;
    }
    
}
