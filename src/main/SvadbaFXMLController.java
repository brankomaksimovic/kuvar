package main;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.AlertBox;

/**
 * FXML Controller class
 *
 * @author Branko
 */
public class SvadbaFXMLController implements Initializable {

    @FXML
    private TextField tekstSastojci;
    @FXML
    private Button dugmeDodaj;
    @FXML
    private Button dugmeObrisi;
    @FXML
    private TableColumn<Sastojci, String> nazivSastojka;
    @FXML
    private TableView<Sastojci> tabelaSastojci;
    private Sastojci dodatSastojak;
    @FXML
    private TableView<Recepti> tabelaRecepti;
    @FXML
    private TableColumn<Recepti, String> nazivRecepta;
    @FXML
    private TextArea prikazRecepta;
    public static ObservableList<Recepti> recepti2;
    @FXML
    private Button dodajReceptDugme;
    @FXML
    private Button obrisiReceptDugme;
    @FXML
    private Button izmeniReceptDugme;
    @FXML
    private Button Izracunaj;
    @FXML
    private Button Obrisi;
    @FXML
    private ComboBox<Recepti> jelo1;
    @FXML
    private ComboBox<Recepti> jelo2;
    @FXML
    private ComboBox<Recepti> jelo3;
    @FXML
    private TextField brojJela1;
    @FXML
    private TextField brojJela2;
    @FXML
    private TextField brojJela3;
    @FXML
    private TextArea prikazRezultata;
    private HashMap<String, Integer> rezultat;
    public static int brojSelektovanogRecepta;
    @FXML
    private Button dodajJosDugme;
    @FXML
    private GridPane nasGridPane;
    private static int brojReda;
    private ArrayList<Recepti> receptiRacun;
    private ArrayList<Integer> kolicineRacun;
    @FXML
    private MenuItem exitMenu;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //Punjenje table sastojci na startu
        tabelaSastojci.setItems(XML.ucitajSastojkeIzXMLa());
        nazivSastojka.setCellValueFactory(new PropertyValueFactory<>("naziv"));

        //Punjenje tabele recepti na startu
        recepti2 = XML.ucitajRecepteizXMLa();
        tabelaRecepti.setItems(recepti2);
        nazivRecepta.setCellValueFactory(new PropertyValueFactory<>("naziv"));
        //Punjenje comboboxa za izbor jela na startu
        jelo1.getItems().addAll(recepti2);
        jelo2.getItems().addAll(recepti2);
        jelo3.getItems().addAll(recepti2);
        jelo1.setPromptText("Izaberite:");
        jelo2.setPromptText("Izaberite:");
        jelo3.setPromptText("Izaberite:");

        //Dodavanje listenera na listu recepata
        tabelaRecepti.getSelectionModel().selectedIndexProperty().addListener(new RowSelectChangeListener());

        brojSelektovanogRecepta = 0;
        brojReda = 3;
    }

    @FXML
    private void dodajSastojak(ActionEvent event) {
        dodatSastojak = new Sastojci(tekstSastojci.getText());
        if (tekstSastojci.getText().isEmpty() || tekstSastojci.getText() == null || proveraSastojka(dodatSastojak.getNaziv())) {
            AlertBox.display("Greska", "Niste nista upisali ili sastojak vec postoji");
        } else {
            tabelaSastojci.getItems().add(dodatSastojak);
            tekstSastojci.clear();
            ObservableList<Sastojci> listaSastojaka = tabelaSastojci.getItems();
            XML.upisiSastojkeUXML(listaSastojaka);

        }
    }

    @FXML
    private void obrisiSastojak(ActionEvent event) {
        ObservableList<Sastojci> izabranSastojak, sviSastojci;
        sviSastojci = tabelaSastojci.getItems();
        izabranSastojak = tabelaSastojci.getSelectionModel().getSelectedItems();
        izabranSastojak.forEach(sviSastojci::remove);
        XML.upisiSastojkeUXML(sviSastojci);
    }

    private boolean proveraSastojka(String naziv) {
        ObservableList<Sastojci> sviSastojci = tabelaSastojci.getItems();
        for (Sastojci s : sviSastojci) {
            if (s.getNaziv().equals(naziv)) {
                return true;
            }
        }
        return false;
    }

    @FXML
    private void dodajRecept(ActionEvent event) throws IOException {
        ReceptFXMLController.reset();
        Parent root = FXMLLoader.load(getClass().getResource("ReceptFXML.fxml"));
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Dodavanje novog recepta");
        stage.setScene(new Scene(root, 450, 450));
        stage.showAndWait();

        if (ReceptFXMLController.getData() != null) {
            Recepti temp = ReceptFXMLController.getData();
            recepti2.add(temp);
            tabelaRecepti.setItems(recepti2);
            XML.upisiRecepteUXML(recepti2);
            jelo1.getItems().clear();
            jelo2.getItems().clear();
            jelo3.getItems().clear();
            jelo1.getItems().addAll(recepti2);
            jelo2.getItems().addAll(recepti2);
            jelo3.getItems().addAll(recepti2);
        }
    }

    @FXML
    private void obrisiRecept(ActionEvent event) {
        ObservableList<Recepti> izabranRecept, sviRecepti;
        sviRecepti = tabelaRecepti.getItems();
        izabranRecept = tabelaRecepti.getSelectionModel().getSelectedItems();
        izabranRecept.forEach(sviRecepti::remove);
        XML.upisiRecepteUXML(sviRecepti);
    }

    @FXML
    private void izmeniRecept(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("izmeniReceptFXML.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(SvadbaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Izmena postojeceg recepta");
        stage.setScene(new Scene(root, 450, 450));
        stage.showAndWait();

        tabelaRecepti.refresh();

        Recepti selektovaniRecept = recepti2.get(brojSelektovanogRecepta);
        StringBuilder builder2 = new StringBuilder();
        builder2.append("Naziv: ").append(selektovaniRecept.getNaziv()).append("\n");

        ArrayList<Sastojak> listaSastojakaTemp = selektovaniRecept.getSastojak();
        for (Sastojak s : listaSastojakaTemp) {
            builder2.append(s.sastojci).append(":   ").append(s.kolicina).append(" ").append(s.mera).append("\n");
        }

        prikazRecepta.setText(builder2.toString());
    }

    @FXML
    private void obrisiJelo(ActionEvent event) {

        for (Node n : nasGridPane.getChildren()) {
            if (n instanceof ComboBox) {
                ComboBox nn = (ComboBox) n;
                nn.getSelectionModel().clearSelection();
            }
            if (n instanceof TextField) {
                ((TextField) n).clear();
            }
        }
    }

    @FXML
    private void izracunajDugme(ActionEvent event) {
        
        for (Node n : nasGridPane.getChildren()) {
            
            if (n instanceof ComboBox) 
            {
                ComboBox nn = (ComboBox) n;         
                if (nn.getSelectionModel().isEmpty())
                    {            
                        AlertBox.display("Greska", "Niste selektovali sva kombo polja!");
                        return;
                    }
            }
            
            if (n instanceof TextField) 
            {
                TextField tt = (TextField) n;               
                if (tt.getText().isEmpty() || tt.getText() == null)
                    {
                        AlertBox.display("Greska", "Niste upisali sva tekstualna polja!");
                        return;
                    }
            }          
        }
         
        receptiRacun = new ArrayList<>();
        kolicineRacun = new ArrayList<>();
        for (Node n : nasGridPane.getChildren()) {
            if (n instanceof ComboBox) {
                ComboBox nn = (ComboBox) n;
                Recepti selectRecept = (Recepti) nn.getSelectionModel().getSelectedItem();
                receptiRacun.add(selectRecept);
            }
            if (n instanceof TextField) {
                String nn = ((TextField) n).getText();
                int nnn = Integer.parseInt(nn);
                kolicineRacun.add(nnn);
            }
        }

        rezultat = new HashMap<>();
        for (Sastojci s : XML.ucitajSastojkeIzXMLa()) {
            for(Mera m: Mera.values())
            {
                String naziv = s.toString().concat(m.name());
                rezultat.put(naziv, 0);
            }
        }

        for (int i = 0; i < receptiRacun.size(); i++) {
            int a = i;
            for (Sastojak s : receptiRacun.get(i).getSastojak()) {
                String naziv1 = s.getSastojci().toString().concat(s.getMera().toString());
                rezultat.computeIfPresent(naziv1, (k, v) -> {
                    v = v + (s.getKolicina() * kolicineRacun.get(a));
                    return v;
                });
            }
        }
        
        //System.out.println(rezultat);

        StringBuilder builder = new StringBuilder();
        builder.append("Potrebno Vam je :").append("\n").append("\n");
        for (Map.Entry<String, Integer> upis : rezultat.entrySet()) {
            if (upis.getValue() != 0) {
                for(Mera me: Mera.values()){
                    
                    Pattern word = Pattern.compile(me.toString());
                    Matcher match = word.matcher(upis.getKey());
                    
                    if (match.find())
                    {                    
                        String mera = me.toString();
                        String sas = upis.getKey().substring(0, upis.getKey().length()-mera.length());
                        builder.append(sas).append(": ").append(upis.getValue()).append(" ").append(mera).append("\n");
                    }                                              
                }              
            }
        }
        prikazRezultata.setText(builder.toString());
    }

    @FXML
    private void dodajJosPolja(ActionEvent event) {
        ComboBox<Recepti> noviCB = new ComboBox<>(recepti2);
        TextField noviTF = new TextField();

        nasGridPane.setConstraints(noviCB, 0, brojReda);
        nasGridPane.setConstraints(noviTF, 1, brojReda);
        nasGridPane.getChildren().add(noviCB);
        nasGridPane.getChildren().add(noviTF);

        noviCB.setMaxWidth(135);
        noviTF.setMaxWidth(90);
        GridPane.setHalignment(noviCB, HPos.CENTER);
        GridPane.setHalignment(noviTF, HPos.CENTER);

        noviCB.setPromptText("Izaberite:");

        nasGridPane.setConstraints(dodajJosDugme, 0, brojReda + 1);
        nasGridPane.setConstraints(Izracunaj, 0, brojReda + 2);
        nasGridPane.setConstraints(Obrisi, 1, brojReda + 2);

        brojReda++;
    }

    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void oNama(ActionEvent event) {
        AlertBox.display("O nama",
        "Motivi su različiti — neko želi da započne programersku karijeru, " +'\n' + "dok je nekome programiranje samo koristan dodatak veštinama u drugoj oblasti, " +'\n' + "ili pak odličan dodatak automatizaciji. Ukoliko imate pitanja za nas, naši mailovi su " +'\n' + "maksimovic.bm@gmail.com i " +'\n' + "andrej.pesic@outlook.com");
        
    }

    @FXML
    private void oAplikaciji(ActionEvent event) {
        AlertBox.display("O aplikaciji","Aplikacija je napravljena u svrhu prezentacije naučenih veština." +'\n' + "Pisana je u JAVA (FX) programskom jeziku sa pomoći SCENE BUILDERa. " +'\n' + "Kod je besplatan za korišćenje.");
    }

    private class RowSelectChangeListener implements ChangeListener {

        @Override
        public void changed(ObservableValue observable, Object oldValue, Object newValue) {

            brojSelektovanogRecepta = (int) (newValue);
            Recepti selektovaniRecept = recepti2.get((int) newValue);

            StringBuilder builder = new StringBuilder();
            builder.append("Naziv: ").append(selektovaniRecept.getNaziv()).append("\n");

            ArrayList<Sastojak> listaSastojakaTemp = selektovaniRecept.getSastojak();
            for (Sastojak s : listaSastojakaTemp) {
                builder.append(s.sastojci).append(":   ").append(s.kolicina).append(" ").append(s.mera).append("\n");
            }

            prikazRecepta.setText(builder.toString());
        }
    }

    /*public ObservableList<Sastojci> vratiIzTabeleSastojke(){      
        ObservableList<Sastojci> sviSastojci = tabelaSastojci.getItems();  
        return sviSastojci;   
    }*/
    public static Recepti vratiReceptZaPromenu() {
        return recepti2.get(brojSelektovanogRecepta);
    }
}
