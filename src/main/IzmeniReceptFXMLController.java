/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.sun.rowset.internal.Row;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import static main.SvadbaFXMLController.brojSelektovanogRecepta;
import static main.SvadbaFXMLController.recepti2;

/**
 * FXML Controller class
 *
 * @author Andrej
 */
public class IzmeniReceptFXMLController implements Initializable {

    @FXML
    private TextField nazivRecepta;
    @FXML
    private Button unesiReceptDugme;
    @FXML
    private Button odustaniReceptDugme;
    @FXML
    private Button dodajPolja;
    private static int brojReda;
    @FXML
    private GridPane gridPaneIzmena;
    @FXML
    private int brojSastojaka;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        brojReda = 0;
        Recepti recept = SvadbaFXMLController.vratiReceptZaPromenu();
        brojSastojaka = recept.getSastojak().size();
        nazivRecepta.setText(recept.getNaziv());

        for (int i = 0; i < brojSastojaka; i++) {
            Label label1 = new Label("Sastojak " + (i + 1));
            Label label2 = new Label("Naziv sastojka");
            Label label3 = new Label("Kolicina");
            Label label4 = new Label("Mera");

            ComboBox<Sastojci> noviCB1 = new ComboBox<>(XML.ucitajSastojkeIzXMLa());
            noviCB1.getSelectionModel().select(recept.getSastojak().get(i).getSastojci());

            TextField noviTF = new TextField();
            noviTF.setId("textField");
            noviTF.setText(Integer.toString(recept.getSastojak().get(i).getKolicina()));

            ComboBox<Mera> noviCB2 = new ComboBox<>();
            noviCB2.setItems(FXCollections.observableArrayList(Mera.values()));
            noviCB2.getSelectionModel().select(recept.getSastojak().get(i).getMera());

            gridPaneIzmena.setConstraints(label1, 1, (brojReda + 3));
            gridPaneIzmena.setConstraints(label2, 1, (brojReda + 4));
            gridPaneIzmena.setConstraints(label3, 1, (brojReda + 5));
            gridPaneIzmena.setConstraints(label4, 1, (brojReda + 6));

            gridPaneIzmena.setConstraints(noviCB1, 2, (brojReda + 4));
            gridPaneIzmena.setConstraints(noviTF, 2, (brojReda + 5));
            gridPaneIzmena.setConstraints(noviCB2, 2, (brojReda + 6));

            Row prazno = new Row((brojReda + 7));
            gridPaneIzmena.addRow(((brojReda + 7)), new Text(""));

            gridPaneIzmena.setConstraints(dodajPolja, 1, (brojReda + 8));
            gridPaneIzmena.setConstraints(unesiReceptDugme, 2, (brojReda + 8));
            gridPaneIzmena.setConstraints(odustaniReceptDugme, 3, (brojReda + 8));

            noviCB1.setPrefWidth(150);
            noviTF.setPrefWidth(150);
            noviCB2.setPrefWidth(150);

            gridPaneIzmena.getChildren().add(label1);
            gridPaneIzmena.getChildren().add(label2);
            gridPaneIzmena.getChildren().add(label3);
            gridPaneIzmena.getChildren().add(label4);
            gridPaneIzmena.getChildren().add(noviCB1);
            gridPaneIzmena.getChildren().add(noviTF);
            gridPaneIzmena.getChildren().add(noviCB2);

            brojReda = brojReda + 5;
        }
    }

    @FXML
    private void izmeniRecept(ActionEvent event) {
       
        for (Node n : gridPaneIzmena.getChildren()) {
            if (n instanceof TextField) 
            {
                TextField tt = (TextField) n;               
                if (tt.getText().isEmpty() || tt.getText() == null)
                    {
                        AlertBox.display("Greska", "Niste upisali sva tekstualna polja!");
                        return;
                    }
            }          
        }
        
        
        String naziv;
        ArrayList<Sastojak> listaSastojakaIzmena = new ArrayList<>();
        ArrayList<Sastojci> sastojciDodavanje = new ArrayList<>();
        ArrayList<Integer> kolicineDodavanje = new ArrayList<>();
        ArrayList<Mera> meraDodavanje = new ArrayList<>();
        

        for (Node n : gridPaneIzmena.getChildren()) {
            if (n instanceof ComboBox) {
                ComboBox nn = (ComboBox) n;
                if (nn.getSelectionModel().getSelectedItem().getClass() == Sastojci.class) {
                    Sastojci izabranSastojci = (Sastojci) nn.getSelectionModel().getSelectedItem();
                    sastojciDodavanje.add(izabranSastojci);
                } else {
                    Mera izabranaMera = (Mera) nn.getSelectionModel().getSelectedItem();
                    meraDodavanje.add(izabranaMera);
                };
            }
            if (n instanceof TextField) {
                if (n.getId().equals("nazivRecepta")) {
                    naziv = nazivRecepta.getText();
                } else {
                    String nn = ((TextField) n).getText();
                    Integer nnn = Integer.parseInt(nn);
                    kolicineDodavanje.add(nnn);
                }
            }
        }

        for (int i = 0; i < sastojciDodavanje.size(); i++) {
            listaSastojakaIzmena.add(new Sastojak(sastojciDodavanje.get(i), kolicineDodavanje.get(i), meraDodavanje.get(i)));
        }     
            recepti2.get(brojSelektovanogRecepta).setNaziv(nazivRecepta.getText());
            recepti2.get(brojSelektovanogRecepta).setSastojak(listaSastojakaIzmena);          
            XML.upisiRecepteUXML(recepti2);
            Stage stage = (Stage) odustaniReceptDugme.getScene().getWindow();
            stage.close();
        }
    
    @FXML
    private void odustaniOdRecepta(ActionEvent event) {
        Stage stage = (Stage) odustaniReceptDugme.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void dodajPolja(ActionEvent event) {
        Label label1 = new Label("Sastojak " + (1 + brojReda / 5));
        Label label2 = new Label("Naziv sastojka");
        Label label3 = new Label("Kolicina");
        Label label4 = new Label("Mera");

        ComboBox<Sastojci> noviCB1 = new ComboBox<>(XML.ucitajSastojkeIzXMLa());

        TextField noviTF = new TextField();
        noviTF.setId("textField");

        ComboBox<Mera> noviCB2 = new ComboBox<>();
        noviCB2.setItems(FXCollections.observableArrayList(Mera.values()));

        gridPaneIzmena.setConstraints(label1, 1, (brojReda + 13));
        gridPaneIzmena.setConstraints(label2, 1, (brojReda + 14));
        gridPaneIzmena.setConstraints(label3, 1, (brojReda + 15));
        gridPaneIzmena.setConstraints(label4, 1, (brojReda + 16));

        gridPaneIzmena.setConstraints(noviCB1, 2, (brojReda + 14));
        gridPaneIzmena.setConstraints(noviTF, 2, (brojReda + 15));
        gridPaneIzmena.setConstraints(noviCB2, 2, (brojReda + 16));

        Row prazno = new Row(brojReda + 17);
        gridPaneIzmena.addRow((brojReda + 17), new Text(""));

        gridPaneIzmena.setConstraints(dodajPolja, 1, (brojReda + 18));
        gridPaneIzmena.setConstraints(unesiReceptDugme, 2, (brojReda + 18));
        gridPaneIzmena.setConstraints(odustaniReceptDugme, 3, (brojReda + 18));

        noviCB1.setPromptText("Izaberite:");
        noviCB2.setPromptText("Izaberite:");

        noviCB1.setPrefWidth(150);
        noviTF.setPrefWidth(150);
        noviCB2.setPrefWidth(150);

        gridPaneIzmena.getChildren().add(label1);
        gridPaneIzmena.getChildren().add(label2);
        gridPaneIzmena.getChildren().add(label3);
        gridPaneIzmena.getChildren().add(label4);
        gridPaneIzmena.getChildren().add(noviCB1);
        gridPaneIzmena.getChildren().add(noviTF);
        gridPaneIzmena.getChildren().add(noviCB2);

        brojReda = brojReda + 5;
    }

}
