/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.sun.org.apache.xpath.internal.operations.Equals;
import java.util.Objects;

/**
 *
 * @author Branko
 */
public class Sastojci {

    public String naziv;

    public Sastojci(String naziv) {
        this.naziv = naziv;
    }

    
    @Override
    public String toString() {
        return naziv;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if (!(obj instanceof Sastojci)) return false;
        if (obj == this) {
            return true;
        }
        
        Sastojci sas = (Sastojci) obj;
        return sas.naziv.equals(naziv);  
        
        
    }

    @Override
    public int hashCode() {
        return Objects.hash(naziv);
    }
}
