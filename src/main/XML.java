package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Text;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Branko
 */
public class XML {

    private TableView<Sastojci> tabelaSastojci;

    public static ObservableList<Sastojci> ucitajSastojkeIzXMLa() {
        ArrayList<Sastojci> listaSastojaka2 = new ArrayList();
        SAXBuilder builder = new SAXBuilder();
        Document doc = new Document();
        Element root = null;

        try {

            doc = builder.build(new File("sastojci.xml"));
            root = doc.getRootElement();
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }

        for (Element curEle : root.getChildren("sastojci")) {
            listaSastojaka2.add(new Sastojci(curEle.getText()));
        }

        ObservableList<Sastojci> listaSastojaka = FXCollections.observableArrayList(listaSastojaka2);
        return listaSastojaka;
    }

    public static void upisiSastojkeUXML(ObservableList<Sastojci> tabelaSastojci) {

        Document doc = new Document();
        Element root = new Element("listaSastojci");
        doc.setRootElement(root);

        try {
            for (Sastojci curSastojak : tabelaSastojci) {
                Element curEle = new Element("sastojci");
                curEle.addContent(new Text(curSastojak.getNaziv()));
                root.addContent(curEle);
            }

            XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
            xmlOutput.output(doc, new FileOutputStream(new File("sastojci.xml")));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ObservableList<Recepti> ucitajRecepteizXMLa() {
        ArrayList<Recepti> arrayListaRecepata = new ArrayList<>();
        ObservableList<Recepti> listaRecepata;
        ArrayList<Sastojak> listaSastojaka = new ArrayList<>();
        SAXBuilder builder = new SAXBuilder();
        Document doc;
        Element root = null;
        try {

            doc = builder.build(new File("recepti.xml"));
            root = doc.getRootElement();
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }

        //ovde se ide kroz listu recepata
        for (Element curEle : root.getChildren("recept")) {
            String nazivRecepta = curEle.getChild("jelo").getText();

            Recepti recept = new Recepti(nazivRecepta);
            listaSastojaka = new ArrayList();
            //ovde se ide kroz listu sastojaka

            for (Element curEle2 : curEle.getChildren("listaSastojaka")) {
                //ovde se ide kroz sastojke
                for (Element curEle3 : curEle2.getChildren("sastojak")) {
                    Sastojak sas = new Sastojak(new Sastojci(curEle3.getChild("sastojci").getText()),
                            Integer.parseInt(curEle3.getChild("kolicina").getText()), Mera.valueOf(curEle3.getChild("mera").getText()));

                    listaSastojaka.add(sas);
                    recept.setSastojak(listaSastojaka);
                }
                arrayListaRecepata.add(recept);
            }
            //System.out.println(recept);
        }
        //System.out.println(arrayListaRecepata);
        listaRecepata = FXCollections.observableArrayList(arrayListaRecepata);
        //System.out.println(listaRecepata);
        return listaRecepata;
    }
    
    public static void upisiRecepteUXML(ObservableList<Recepti> recepti)
    {
        Document doc = new Document();
        Element root = new Element("recepti");
        doc.setRootElement(root);

        try {
            for (Recepti curRecept : recepti)
            {
                Element E1 = new Element("recept");
                root.addContent(E1);
                Element E2 = new Element("jelo");
                E2.addContent(new Text(curRecept.getNaziv()));
                Element E3 = new Element("listaSastojaka");               
                
                for (int i=0; i<curRecept.getSastojak().size();i++)
                {
                    Sastojak sas = new Sastojak(curRecept.getSastojak().get(i).sastojci,
                                                curRecept.getSastojak().get(i).getKolicina(),
                                                curRecept.getSastojak().get(i).getMera());
                    Element E4 = new Element("sastojak");
                    
                    Element sastojci = new Element("sastojci");
                    Element kolicina = new Element("kolicina");
                    Element mera = new Element("mera");
                    
                    sastojci.addContent(new Text(sas.sastojci.getNaziv()));
                    kolicina.addContent(new Text(Integer.toString(sas.getKolicina())));
                    mera.addContent(new Text(sas.getMera().toString()));
                    
                    E4.addContent(sastojci);              
                    E4.addContent(kolicina);
                    E4.addContent(mera);
                    
                    E3.addContent(E4);
                }
                
                E1.addContent(E2);
                E1.addContent(E3);
                
            }

            XMLOutputter xmlOutput = new XMLOutputter(Format.getPrettyFormat());
            xmlOutput.output(doc, new FileOutputStream(new File("recepti.xml")));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
