/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.sun.java.swing.plaf.windows.resources.windows;
import com.sun.rowset.internal.Row;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.swing.DefaultComboBoxModel;
import static main.SvadbaFXMLController.recepti2;

/**
 * FXML Controller class
 *
 * @author Andrej
 */
public class ReceptFXMLController implements Initializable {

    @FXML
    private Button unesiReceptDugme;
    @FXML
    private Button odustaniReceptDugme;
    @FXML
    private ComboBox<Sastojci> ReceptComboBox1;
    @FXML
    private ComboBox<Sastojci> ReceptComboBox2;
    @FXML
    private ComboBox<Sastojci> ReceptComboBox3;
    @FXML
    private TextField nazivRecepta;
    @FXML
    private TextField kolicina1;
    @FXML
    private ComboBox<Mera> mera1;
    @FXML
    private TextField kolicina2;
    @FXML
    private ComboBox<Mera> mera2;
    @FXML
    private TextField kolicina3;
    @FXML
    private ComboBox<Mera> mera3;
    private static Recepti uneseniRecept;
    @FXML
    private Button dodajPolja;
    @FXML
    private static int brojRedaRecept;
    @FXML
    private GridPane receptGridPane;
    @FXML
    private ArrayList<Sastojci> sastojciDodavanje;
    @FXML
    private ArrayList<Integer> kolicineDodavanje;
    @FXML
    private ArrayList<Mera> meraDodavanje;
    @FXML
    private String naziv;
    @FXML
    private ArrayList<Sastojak> tempSas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ReceptComboBox1.getItems().addAll(XML.ucitajSastojkeIzXMLa());
        ReceptComboBox2.getItems().addAll(XML.ucitajSastojkeIzXMLa());
        ReceptComboBox3.getItems().addAll(XML.ucitajSastojkeIzXMLa());
        ReceptComboBox1.setPromptText("Izaberite:");
        ReceptComboBox2.setPromptText("Izaberite:");
        ReceptComboBox3.setPromptText("Izaberite:");

        mera1.getItems().addAll(Mera.values());
        mera2.getItems().addAll(Mera.values());
        mera3.getItems().addAll(Mera.values());

        mera1.setPromptText("Izaberite:");
        mera2.setPromptText("Izaberite:");
        mera3.setPromptText("Izaberite:");

        brojRedaRecept = 5;
    }

    @FXML
    public void unesiRecept(ActionEvent event) {

        for (Node n : receptGridPane.getChildren()) {
            
            if (n instanceof ComboBox) 
            {
                ComboBox nn = (ComboBox) n;
                if (nn.getSelectionModel().isEmpty())
                    {
                        AlertBox.display("Greska", "Niste selektovali sva kombo polja!");
                        return;
                    }
            }
            
            if (n instanceof TextField) 
            {
                TextField tt = (TextField) n;               
                if (tt.getText().isEmpty() || tt.getText() == null)
                    {
                        AlertBox.display("Greska", "Niste upisali sva tekstualna polja!");
                        return;
                    }
            }          
        }

            sastojciDodavanje = new ArrayList<>();
            kolicineDodavanje = new ArrayList<>();
            meraDodavanje = new ArrayList<>();
            tempSas = new ArrayList<>();

            for (Node n : receptGridPane.getChildren()) {
                if (n instanceof ComboBox) {
                    ComboBox nn = (ComboBox) n;
                    if (nn.getSelectionModel().getSelectedItem().getClass() == Sastojci.class) {
                        Sastojci izabranSastojci = (Sastojci) nn.getSelectionModel().getSelectedItem();
                        sastojciDodavanje.add(izabranSastojci);
                    } else {
                        Mera izabranaMera = (Mera) nn.getSelectionModel().getSelectedItem();
                        meraDodavanje.add(izabranaMera);
                    };
                }
                if (n instanceof TextField) {
                    if (n.getId().equals("nazivRecepta")) {
                        naziv = nazivRecepta.getText();
                    } else {
                        String nn = ((TextField) n).getText();
                        Integer nnn = Integer.parseInt(nn);
                        kolicineDodavanje.add(nnn);
                    }
                }
            }

            for (int i = 0; i < sastojciDodavanje.size(); i++) {
                tempSas.add(new Sastojak(sastojciDodavanje.get(i), kolicineDodavanje.get(i), meraDodavanje.get(i)));

            }
            uneseniRecept = new Recepti(naziv, tempSas);

            Stage stage = (Stage) odustaniReceptDugme.getScene().getWindow();
            stage.close();

        }

        @FXML
        private void odustaniOdRecepta
        (ActionEvent event
        
            ) {
        Stage stage = (Stage) odustaniReceptDugme.getScene().getWindow();
            stage.close();
        }

    

    public static Recepti getData() {
        return uneseniRecept;
    }

    public static Recepti reset() {
        return uneseniRecept = null;
    }

    @FXML
    private void dodajPoljaZaUnos(ActionEvent event) {
        Label label1 = new Label("Sastojak " + (3 + brojRedaRecept / 5));
        Label label2 = new Label("Naziv sastojka");
        Label label3 = new Label("Kolicina");
        Label label4 = new Label("Mera");
        ComboBox<Sastojci> noviCB1 = new ComboBox<>(XML.ucitajSastojkeIzXMLa());
        TextField noviTF = new TextField();

        noviTF.setId("textField");

        ComboBox<Mera> noviCB2 = new ComboBox<>();
        noviCB2.setItems(FXCollections.observableArrayList(Mera.values()));

        receptGridPane.setConstraints(label1, 1, (brojRedaRecept + 13));
        receptGridPane.setConstraints(label2, 1, (brojRedaRecept + 14));
        receptGridPane.setConstraints(label3, 1, (brojRedaRecept + 15));
        receptGridPane.setConstraints(label4, 1, (brojRedaRecept + 16));

        receptGridPane.setConstraints(noviCB1, 2, (brojRedaRecept + 14));
        receptGridPane.setConstraints(noviTF, 2, (brojRedaRecept + 15));
        receptGridPane.setConstraints(noviCB2, 2, (brojRedaRecept + 16));

        Row prazno = new Row(brojRedaRecept + 17);
        receptGridPane.addRow((brojRedaRecept + 17), new Text(""));

        receptGridPane.setConstraints(dodajPolja, 1, (brojRedaRecept + 18));
        receptGridPane.setConstraints(unesiReceptDugme, 2, (brojRedaRecept + 18));
        receptGridPane.setConstraints(odustaniReceptDugme, 3, (brojRedaRecept + 18));

        noviCB1.setPromptText("Izaberite:");
        noviCB2.setPromptText("Izaberite:");

        noviCB1.setPrefWidth(150);
        noviTF.setPrefWidth(150);
        noviCB2.setPrefWidth(150);

        receptGridPane.getChildren().add(label1);
        receptGridPane.getChildren().add(label2);
        receptGridPane.getChildren().add(label3);
        receptGridPane.getChildren().add(label4);
        receptGridPane.getChildren().add(noviCB1);
        receptGridPane.getChildren().add(noviTF);
        receptGridPane.getChildren().add(noviCB2);

        brojRedaRecept = brojRedaRecept + 5;
    }

}
